package net.lyxnx.simplerest.example

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import net.lyxnx.simplerest.example.request.ExampleGetTestRequest

fun exampleTestRequest() {
    GlobalScope.launch {
        ExampleGetTestRequest("Example body").execute {

        }
    }
}
